import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    signal connectionRequested(string host, string password)

    title: qsTr("VNC Client")

    ColumnLayout {
        anchors { left: parent.left; right: parent.right; margins: 8 }

        Label {
            Layout.fillWidth: true
            text: qsTr("Enter the URL of the VNC server to connect to:")
            wrapMode: Text.WordWrap
        }

        TextFieldWithContextMenu {
            id: urlField
            Layout.fillWidth: true
            placeholderText: qsTr("VNC server")
            selectByMouse: true
            onAccepted: activate()

            function activate() {
                root.connectionRequested(text, passwordField.text)
            }
        }

        TextFieldWithContextMenu {
            id: passwordField
            Layout.fillWidth: true
            placeholderText: qsTr("Password (optional)")
            echoMode: TextInput.PasswordEchoOnEdit
            selectByMouse: true
            onAccepted: urlField.activate();
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("Connect")
            highlighted: true
            onClicked: urlField.activate()
        }
    }
}
