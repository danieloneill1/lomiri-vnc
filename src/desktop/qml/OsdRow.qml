import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

RowLayout {
    id: root

    signal toggleFullScreenRequested()
    signal moveViewRequested()

    anchors.fill: parent

    Item { Layout.fillWidth: true }

    OsdButton {
        Layout.alignment : Qt.AlignRight
        iconSource: "qrc:/icons/move-view"
        onClicked: root.moveViewRequested()
    }

    OsdButton {
        Layout.alignment : Qt.AlignRight
        iconSource: "qrc:/icons/fullscreen"
        onClicked: root.toggleFullScreenRequested()
    }
}
