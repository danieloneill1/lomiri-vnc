/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LomiriVNC.
 *
 * LomiriVNC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LomiriVNC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LomiriVNC.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vnc_client.h"

#include <QByteArrayList>
#include <QDebug>
#include <QImage>
#include <QList>
#include <QMouseEvent>
#include <QPointF>
#include <QQuickItem>
#include <QScopedPointer>
#include <QSocketNotifier>
#include <rfb/rfbclient.h>

using namespace LomiriVNC;

namespace LomiriVNC {

class VncClientPrivate {
    Q_DECLARE_PUBLIC(VncClient)

public:
    VncClientPrivate(VncClient *q);
    ~VncClientPrivate();

    static void *dataTag();
    static void vncLog(const char *format, ...);
    static void vncError(const char *format, ...);
    static void gotFrameBufferUpdate(rfbClient *cl,
                                     int x, int y, int w, int h);
    static char *GetPassword(rfbClient *cl);
    static rfbBool mallocFrameBuffer(rfbClient* client);

    static int qtToRfb(Qt::MouseButtons buttons);

    void onUpdate(int x, int y, int w, int h);
    void onResize();
    char *getPassword();

    bool connectToServer(const QString &host, const QString &password);
    void disconnect();

    void onSocketActivated();

    void sendMouseEvent(const QPointF &pos, Qt::MouseButtons buttons);

private:
    QScopedPointer<QSocketNotifier> m_notifier;
    int m_bytesPerPixel;
    QImage m_image;
    QList<QQuickItem*> m_viewers;
    QString m_password;
    rfbClient *m_client;
    VncClient *q_ptr;
};

} // namespace

VncClientPrivate::VncClientPrivate(VncClient *q):
    m_bytesPerPixel(4),
    m_client(nullptr),
    q_ptr(q)
{
    rfbClientLog = vncLog;
    rfbClientErr = vncError;
}

VncClientPrivate::~VncClientPrivate()
{
    disconnect();
}

void *VncClientPrivate::dataTag()
{
    return reinterpret_cast<void*>(dataTag);
}

void VncClientPrivate::vncLog(const char *format, ...)
{
    va_list args;
	va_start(args, format);
    QString message = QString::vasprintf(format, args);
	va_end(args);

    qDebug() << "VNC:" << message.trimmed();
}

void VncClientPrivate::vncError(const char *format, ...)
{
    va_list args;
	va_start(args, format);
    QString message = QString::vasprintf(format, args);
	va_end(args);

    qWarning() << "VNC:" << message.trimmed();
}

void VncClientPrivate::gotFrameBufferUpdate(rfbClient *client,
                                            int x, int y, int w, int h)
{
    void *ptr = rfbClientGetClientData(client, dataTag());
    static_cast<VncClientPrivate*>(ptr)->onUpdate(x, y, w, h);
}

char *VncClientPrivate::GetPassword(rfbClient *client)
{
    void *ptr = rfbClientGetClientData(client, dataTag());
    return static_cast<VncClientPrivate*>(ptr)->getPassword();
}

rfbBool VncClientPrivate::mallocFrameBuffer(rfbClient* client)
{
    void *ptr = rfbClientGetClientData(client, dataTag());
    static_cast<VncClientPrivate*>(ptr)->onResize();
    return true;
}

int VncClientPrivate::qtToRfb(Qt::MouseButtons buttons)
{
    int ret = 0;
    if (buttons & Qt::LeftButton) ret |= rfbButton1Mask;
    if (buttons & Qt::RightButton) ret |= rfbButton2Mask;
    if (buttons & Qt::MiddleButton) ret |= rfbButton3Mask;
    return ret;
}

void VncClientPrivate::onUpdate(int x, int y, int w, int h)
{
    Q_UNUSED(x);
    Q_UNUSED(y);
    Q_UNUSED(w);
    Q_UNUSED(h);

    for (QQuickItem *viewer: m_viewers) {
        // TODO: update only the changed area
        viewer->update();
    }
}

void VncClientPrivate::onResize()
{
    int width = m_client->width;
    int height = m_client->height;
    qDebug() << Q_FUNC_INFO << width << height;

    m_client->updateRect.x = m_client->updateRect.y = 0;
    m_client->updateRect.w = width;
    m_client->updateRect.h = height;

    m_image = QImage(m_client->width, m_client->height, QImage::Format_RGB32);
    m_client->frameBuffer = m_image.bits();
    m_client->width = m_image.bytesPerLine() / m_bytesPerPixel;
    m_client->format.bitsPerPixel = m_image.depth();
    m_client->format.redShift=16;
    m_client->format.greenShift=8;
    m_client->format.blueShift=0;
    m_client->format.redMax=0xff;
    m_client->format.greenMax=0xff;
    m_client->format.blueMax=0xff;
    bool ok = SetFormatAndEncodings(m_client);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not set format to server";
    }
}

char *VncClientPrivate::getPassword()
{
	return strdup( m_password.toStdString().c_str() );
}

bool VncClientPrivate::connectToServer(const QString &host, const QString &password)
{
    Q_Q(VncClient);

    if (m_client) {
        disconnect();
    }

    m_password = QString(password);

    m_client = rfbGetClient(8, 3, m_bytesPerPixel);
    m_client->MallocFrameBuffer = mallocFrameBuffer;
    m_client->GotFrameBufferUpdate = gotFrameBufferUpdate;
    m_client->GetPassword = GetPassword;
    rfbClientSetClientData(m_client, dataTag(), this);

    QByteArrayList arguments = {
        "lomiri-vnc",
    };
    arguments.append(host.toUtf8());
    int argc = arguments.count();
    QVector<char *> argv;
    argv.reserve(argc + 1);
    for (const QByteArray &a: arguments) {
        argv.append((char*)a.constData());
    }
    argv.append(nullptr);

    bool ok = rfbInitClient(m_client, &argc, argv.data());
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not initialize rfbClient";
        m_client = nullptr;
        return false;
    }

    m_notifier.reset(new QSocketNotifier(m_client->sock,
                                         QSocketNotifier::Read));
    QObject::connect(m_notifier.data(), &QSocketNotifier::activated,
                     q, [this]() { onSocketActivated(); });
    Q_EMIT q->connectionStatusChanged();
    return true;
}

void VncClientPrivate::disconnect()
{
    Q_Q(VncClient);

    m_notifier.reset();
    if (m_client) {
        rfbClientCleanup(m_client);
        m_client = nullptr;
    }

    Q_EMIT q->connectionStatusChanged();
}

void VncClientPrivate::onSocketActivated()
{
    bool ok = HandleRFBServerMessage(m_client);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "RFB failed to handle message";
    }
}

void VncClientPrivate::sendMouseEvent(const QPointF &pos,
                                      Qt::MouseButtons buttons)
{
    if (Q_UNLIKELY(!m_client)) {
        qWarning() << "Not connected";
        return;
    }
    SendPointerEvent(m_client, pos.x(), pos.y(), qtToRfb(buttons));
}

VncClient::VncClient(QObject *parent):
    QObject(parent),
    d_ptr(new VncClientPrivate(this))
{
}

VncClient::~VncClient() = default;

bool VncClient::isConnected() const
{
    Q_D(const VncClient);
    return d->m_client != nullptr;
}

void VncClient::addViewer(QQuickItem *viewer)
{
    Q_D(VncClient);
    d->m_viewers.append(viewer);
}

void VncClient::removeViewer(QQuickItem *viewer)
{
    Q_D(VncClient);
    d->m_viewers.removeAll(viewer);
}

const QImage &VncClient::image() const
{
    Q_D(const VncClient);
    return d->m_image;
}

bool VncClient::connectToServer(const QString &host, const QString &password)
{
    Q_D(VncClient);
    return d->connectToServer(host, password);
}

void VncClient::disconnect()
{
    Q_D(VncClient);
    return d->disconnect();
}

void VncClient::sendMouseEvent(const QPointF &pos, Qt::MouseButtons buttons)
{
    Q_D(VncClient);
    return d->sendMouseEvent(pos, buttons);
}
